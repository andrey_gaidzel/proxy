const express = require("express");
const path = require('path');
const app = express();
let requesT = require('request');


app.get("/onliner", function(request, response){
  requesT.get(
    'https://ak.api.onliner.by/search/apartments?rent_type%5B%5D=1_room&rent_type%5B%5D=2_rooms&rent_type%5B%5D=3_rooms&rent_type%5B%5D=4_rooms&rent_type%5B%5D=5_rooms&rent_type%5B%5D=6_rooms&price%5Bmin%5D=50&price%5Bmax%5D=285&currency=usd&bounds%5Blb%5D%5Blat%5D=53.68837277964328&bounds%5Blb%5D%5Blong%5D=27.220410045418088&bounds%5Brt%5D%5Blat%5D=54.00824936875704&bounds%5Brt%5D%5Blong%5D=27.5747888157177&page=1&v=0.46631406015571764',
    { json: { key: 'value' } },
    function (error, res, body) {
      if (res && res.result) {
        console.log('res_', res);
        response.send({result: res.result.body});
      }
      if (!error && response.statusCode === 200) {
        console.log(body);
      }
    }
  );
});

const allowedExt = [ '.js', '.ico', '.css', '.png', '.jpg', '.woff2', '.woff', '.ttf', '.svg', ];

app.get("/", function(request, response) {
  if (allowedExt.filter(ext => request.url.indexOf(ext) > 0).length > 0) {
    response.sendFile(path.resolve(`proxy-app/${request.url}`));
  } else {
    response.sendFile(path.resolve('proxy-app/index.html'));
  }
});

app.get("*", function(request, response) {
  response.sendFile(path.resolve(`proxy-app/${request.url}`));
});

app.listen(3000, "127.0.0.1",()=>{
  console.log("Сервер начал прослушивание запросов");
});









// let http = require('http');
// let fs = require('fs');
// let path = require('path');
//
// http.createServer(function (request, response) {
//   console.log('request ', request.url);
//
//   let filePath = '.' + request.url;
//   if (filePath == './') {
//     filePath = './proxy-app/index.html';
//   }
//
//   let extname = String(path.extname(filePath)).toLowerCase();
//   let mimeTypes = {
//     '.html': 'text/html',
//     '.js': 'text/javascript',
//     '.css': 'text/css',
//     '.json': 'application/json',
//     '.png': 'image/png',
//     '.jpg': 'image/jpg',
//     '.gif': 'image/gif',
//     '.wav': 'audio/wav',
//     '.mp4': 'video/mp4',
//     '.woff': 'application/font-woff',
//     '.ttf': 'application/font-ttf',
//     '.eot': 'application/vnd.ms-fontobject',
//     '.otf': 'application/font-otf',
//     '.svg': 'application/image/svg+xml'
//   };
//
//   let contentType = mimeTypes[extname] || 'application/octet-stream';
//
//   fs.readFile(filePath, function(error, content) {
//     if (error) {
//       if(error.code == 'ENOENT') {
//         fs.readFile('./404.html', function(error, content) {
//           response.writeHead(200, { 'Content-Type': contentType });
//           response.end(content, 'utf-8');
//         });
//       }
//       else {
//         response.writeHead(500);
//         response.end('Sorry, check with the site admin for error: '+error.code+' ..\n');
//         response.end();
//       }
//     }
//     else {
//       response.writeHead(200, { 'Content-Type': contentType });
//       response.end(content, 'utf-8');
//     }
//   });
//
// }).listen(3000, "127.0.0.1",()=>{
//   console.log("Сервер начал прослушивание запросов");
// });
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})

export class DataService {

  constructor(private http: HttpClient) {
  }

  public getOnliner(): Observable<any> {
    const headerDict = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Headers': 'origin, content-type, accept',
    };

    const requestOptions = {
      headers: new HttpHeaders(headerDict),
    };
    return this.http.get('http://localhost:3000', requestOptions );
  }
}

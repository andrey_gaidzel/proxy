import { Component } from '@angular/core';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private dataService: DataService) {
  }

  public onClick() {
    this.dataService.getOnliner()
      .subscribe(res => {
        console.log(123, res);
      })
  }
}
